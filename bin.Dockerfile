FROM yunnysunny/golang:1.18 AS build-stage
COPY . /opt
WORKDIR /opt

RUN mkdir -p bin && go mod tidy && go build -o bin/use-my

FROM scratch AS export-stage
COPY --from=build-stage /opt/bin/use-my /