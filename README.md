## 使用说明
首先需要确保正确设置了环境变量，否则需要首先运行如下命令

go env -w GOPRIVATE=gitlab.com

go.mod 文件需要做特殊配置：

```
require gitlab.com/myprivate7/mod-my v0.0.1

```

其中 v0.0.1 是项目 mod-my 中的一个 git tag 的名字。

本地开发的时候，如果使用 ssh 模式和 gitlab 通信，首先需要通过 `git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com"` 来完成 git 配置，保证 go 的命令行使用 ssh 模式和 gitlab 通信。如果使用 https 模式和 gitlab 通信，则需要保证配置了 https 的免登录配置。

然后通过 `go mod download` 或者 `go mod tidy` 即可完成安装（后者需要保证 go.mod require 的包在代码中都做 import 了，否则会被清理掉）。

## 常见错误

如果现在过程中出现如下错误：

```
go mod download: gitlab.com/myprivate7/mod-my.git@v0.0.1: invalid version: git ls-remote -q origin in /root/go/pkg/mod/cache/vcs/49e4f4ef52d3227f1c09bb8a8db5321ccf5cd277662d5a9ad607ffd2ff57b32d: exit status 128:
        fatal: unable to connect to gitlab.com:
        gitlab.com[0: 10.200.250.10]: errno=Connection refused
```

通过提示信息上来看是版本号找不到，但是真实的原因可能并不如此。需要做两方面的检查，一方面确认 git 仓库上是否有 `v0.0.1` 这个 tag；另一方面也需要确认当前系统中配置的 ssh 私钥是否有访问 mod-test 这个项目的权限，如果没有权限，同样会报 `invalid version` 这个错误。

> 本项目作为公开项目，理论上应该不会出现上述问题，不过如果你的 build-stage 阶段中 ssh 配置没有生效的话，也会出现上述情况。如果你使用自己的私有仓库，更需要留意权限方面的问题。